import { useRef, useState } from "react";
import { Modal, Form, Button, Alert, Spinner } from "react-bootstrap";
import GoogleButton from "react-google-button";

import { useSelector, useDispatch } from "react-redux";

import { modalActions } from "../../stores/modal";
import useAuth, { AUTH_ACTIONS } from "../../hooks/use-auth";

const LoginModal = () => {
  const dispatch = useDispatch();

  const [isNewUser, setIsNewUser] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [status, setStatus] = useState({
    isVisible: false,
    type: null,
    title: null,
    message: null,
  });

  const inputEmail = useRef();
  const inputPassword = useRef();
  const { loginHandler } = useAuth();

  const isShow = useSelector((state) => state.modal.isShow);

  let timer = null;

  console.log("LOGIN MODAL");

  const resetModal = () => {
    setStatus({
      isVisible: true,
      type: "success",
      title: "SUCCESS",
      message: "Login Success",
    });
    dispatch(modalActions.toggleShow({ loginModal: false }));
    setStatus({
      isVisible: false,
      type: null,
      title: null,
      message: null,
    });
    setIsLoading(false);
    setIsNewUser(false);
  };

  const newUserHandler = () => {
    setIsNewUser(!isNewUser);
  };

  const toggleHandler = () => {
    dispatch(modalActions.toggleShow({ loginModal: false }));
    resetModal();
  };

  const onSuccessHandler = () => {
    setStatus({
      isVisible: true,
      type: "success",
      title: "Success",
      message: "Login Success",
    });
    timer = setTimeout((timer) => {
      clearTimeout(timer);
      resetModal();
      dispatch(modalActions.toggleShow({ loginModal: false }));
    }, 1500);
  };

  const onErrorHandler = (error) => {
    setStatus({
      isVisible: true,
      type: "danger",
      title: "Oh snap, you got an error!",
      message: "Something wrong during login",
    });
    console.log(error.code);
    setIsLoading(false);
  };

  const googleHandler = () => {
    setIsLoading(true);
    loginHandler(AUTH_ACTIONS.GOOGLE, {}, onSuccessHandler, onErrorHandler);
  };

  const submitHandler = (e) => {
    e.preventDefault();

    const enteredEmail = inputEmail.current.value;
    const enteredPassword = inputPassword.current.value;

    setIsLoading(true);

    if (isNewUser) {
      loginHandler(
        AUTH_ACTIONS.CREATE_PASSWORD,
        { email: enteredEmail, password: enteredPassword },
        onSuccessHandler,
        onErrorHandler
      );
    } else {
      loginHandler(
        AUTH_ACTIONS.PASSWORD,
        { email: enteredEmail, password: enteredPassword },
        onSuccessHandler,
        onErrorHandler
      );
    }
  };

  return (
    <Modal show={isShow.loginModal} onHide={toggleHandler} centered>
      <Modal.Header closeButton>
        <Modal.Title>Login</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Alert
          show={status.isVisible}
          variant={status.type}
          onClose={() => {
            setStatus({ ...status, isVisible: false });
          }}
          dismissible
        >
          <Alert.Heading>{status.title}</Alert.Heading>
          <p>{status.message}</p>
        </Alert>
        <Form onSubmit={submitHandler}>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              ref={inputEmail}
              type="email"
              placeholder="name@example.com"
              autoComplete="email"
              autoFocus
              disabled={isLoading}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              ref={inputPassword}
              type="password"
              placeholder="Password"
              autoComplete="current-password"
              disabled={isLoading}
            />
          </Form.Group>
          <Form.Group>
            <Button
              variant="secondary"
              onClick={newUserHandler}
              disabled={isLoading}
            >
              {!isNewUser ? "Create Account" : "Cancel"}
            </Button>
            <Button
              style={{ float: "right" }}
              variant={!isNewUser ? "primary" : "warning"}
              type="submit"
              disabled={isLoading}
            >
              {isLoading && (
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
              )}
              {!isNewUser ? "Login" : "Create"}
            </Button>
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer className="justify-content-center">
        <GoogleButton onClick={googleHandler} disabled={isLoading} />
      </Modal.Footer>
    </Modal>
  );
};

export default LoginModal;
