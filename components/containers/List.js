import {
  push,
  ref,
  set,
  off,
  onValue,
} from "firebase/database";
import { useContext, useEffect, useState } from "react";
import { ListGroup, Button } from "react-bootstrap";
import AuthContext from "../../contexts/auth-context";
import FirebaseContext from "../../contexts/firebase-context";

const List = (props) => {
  const { firebaseApp: app, firebaseDb: db } = useContext(FirebaseContext);
  const uid = useContext(AuthContext).userData.user.uid;
  const [eventList, setEventList] = useState({});

  let listItemView = [];

  console.log("LIST COMPONENT");

  const eventListRef = ref(db, "events/" + uid);

  const newEventHandler = () => {
    const newEventRef = push(eventListRef);
    set(newEventRef, {
      title: "TEST 123",
      options: {
        auth: "PASS_PHRASE",
      },
    })
      .then(() => console.log("ADDED"))
      .catch((reason) => console.log(reason));
  };

  useEffect(() => {
    const eventListRef = ref(db, "events/" + uid);
    console.log("USE EFFECT LIST");
    onValue(
      eventListRef,
      (snapshot) => {
        setEventList(snapshot.val());
      },
      (error) => console.log(error)
    );
    return () => {
      console.log("CLEAN UP LIST");
      off(eventListRef);
    };
  }, []);

  for (const key in eventList) {
    if (Object.hasOwnProperty.call(eventList, key)) {
      const e = eventList[key];
      listItemView.push(
        <ListGroup.Item key={key}>{e.title}</ListGroup.Item>
      );
    }
  }

  return (
    <>
      <Button onClick={newEventHandler}>ADD</Button>
      <ListGroup>{listItemView}</ListGroup>
    </>
  );
};

export default List;
