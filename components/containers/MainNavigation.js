import Link from "next/link";

import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";

import { useSelector, useDispatch } from "react-redux";
import { modalActions } from "../../stores/modal";
import useAuth from "../../hooks/use-auth";

const MainNavigation = () => {
  // const { isLoggedIn, userData } = useContext(AuthContext);
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  const toggleHandler = () => {
    dispatch(modalActions.toggleShow({ loginModal: true }));
  };

  const { logoutHandler } = useAuth();

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Link href="/" passHref>
          <Navbar.Brand>Testing App</Navbar.Brand>
        </Link>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            {isLoggedIn && (
              <NavDropdown title="Events" id="collasible-nav-dropdown">
                <Link href="/events" passHref>
                  <NavDropdown.Item>List</NavDropdown.Item>
                </Link>
                <Link href="/events/new" passHref>
                  <NavDropdown.Item>Create</NavDropdown.Item>
                </Link>
                <Link href="/events/detail/123123" passHref>
                  <NavDropdown.Item>Detail</NavDropdown.Item>
                </Link>
              </NavDropdown>
            )}
          </Nav>
          <Nav>
            {isLoggedIn && (
              <NavDropdown title="Account" id="collasible-nav-dropdown">
                <NavDropdown.Item>Profile</NavDropdown.Item>
                <NavDropdown.Item onClick={logoutHandler}>
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            )}
            {!isLoggedIn && <Nav.Link onClick={toggleHandler}>Login</Nav.Link>}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default MainNavigation;
