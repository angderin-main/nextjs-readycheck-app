import { useEffect } from "react";
import { useRouter } from "next/router";
import { Container } from "react-bootstrap";

import { useDispatch } from "react-redux";
import useFirebase from "../../hooks/use-firebase";
import { authActions } from "../../stores/auth";

const Layout = (props) => {
  const { firebaseAuth: auth } = useFirebase();
  const dispatch = useDispatch();

  const router = useRouter();

  useEffect(() => {
    console.log("AUTH CONTEXT PROVIDER USE EFFECT");

    const unsubcribe = auth.onAuthStateChanged((user) => {
      if (user) {
        // console.log(user);
        dispatch(authActions.isLogin());
        if (router.pathname === "/") {
          router.replace("/events");
        }
      } else {
        if (router.pathname !== "/") {
          router.replace("/");
        }
      }
    });
    return () => {
      unsubcribe();
    };
  }, [dispatch, router, auth]);

  return (
    <Container style={{ backgroundColor: "#F2F2F2" }}>
      {props.children}
    </Container>
  );
};

export default Layout;
