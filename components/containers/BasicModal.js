import { Modal } from "react-bootstrap";

import { useSelector, useDispatch } from "react-redux";
import { modalActions } from "../../stores/modal";

const BasicModal = (props) => {
  const displayData = props
    ? {
        title: null,
        body: { className: null, content: null },
        footer: { className: null, content: null },
      }
    : props;
  const dispatch = useDispatch();

  const { isShow } = useSelector((state) => state.modal);

  const toggleHandler = () => {
    dispatch(modalActions.toggleShow());
  };

  return (
    <Modal show={isShow} onHide={toggleHandler} centered>
      <Modal.Header closeButton>
        <Modal.Title>{displayData.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body className={displayData.body.className} id="modal-body">
        {displayData.body.content}
      </Modal.Body>
      <Modal.Footer className={displayData.footer.className} id="modal-footer">
        {displayData.footer.content}
      </Modal.Footer>
    </Modal>
  );
};

export default BasicModal;
