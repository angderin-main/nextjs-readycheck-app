import { createSlice } from "@reduxjs/toolkit";

export const initializeAuthState = {
  isLoggedIn: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState: initializeAuthState,
  reducers: {
    isLogin(state) {
      state.isLoggedIn = true;
    },
    isLogout(state) {
      state.isLoggedIn = false;
    },
  },
});

export const authActions = authSlice.actions;

export default authSlice.reducer;
