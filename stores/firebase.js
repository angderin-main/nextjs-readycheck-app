import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { createSlice } from "@reduxjs/toolkit";

const firebaseConfig = {
  apiKey: "AIzaSyAci6lmByoG9Y2CvsSccCx5ZIlzAI2KwZ0",
  authDomain: "readycheck-app.firebaseapp.com",
  // The value of `databaseURL` depends on the location of the database
  databaseURL:
    "https://readycheck-app-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "readycheck-app",
  storageBucket: "readycheck-app.appspot.com",
  messagingSenderId: "46728756311",
  appId: "APP_ID",
  // For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
  measurementId: "G-MEASUREMENT_ID",
};

const app = initializeApp(firebaseConfig, "ready-check");
const db = getDatabase(app);
const auth = getAuth(app);

const initFirebaseState = {
  firebaseApp: app,
  firebaseDb: db,
  firebaseAuth: auth,
};

const firebaseSlice = createSlice({
  name: "firebase",
  initialState: initFirebaseState,
  reducers: {},
});

export const firebaseActions = firebaseSlice.actions;

export default firebaseSlice.reducer;
