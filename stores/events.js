import { createSlice } from "@reduxjs/toolkit";

export const initializeEventState = {
  title: "",
  options: {},
  party: {},
  test: {}
};

export const initializeEventsState = {};

const eventSlice = createSlice({
  name: "event",
  initialState: initializeEventsState,
  reducers: {
    setEvents(state, action) {
      state.test = action.payload;
    },
    addEvent(state) {},
  },
});

export const eventActions = eventSlice.actions;

export default eventSlice.reducer;
