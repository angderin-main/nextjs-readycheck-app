import { configureStore } from "@reduxjs/toolkit";
import auth from "./auth";
import modal from "./modal";
import events from "./events";

const store = configureStore({
  reducer: {
    auth: auth,
    modal: modal,
    events: events
  },
});

export default store;
