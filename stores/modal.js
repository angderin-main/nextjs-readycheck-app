import { createSlice } from "@reduxjs/toolkit";

const initializeModalState = {
  isShow: {},
};

const modalSlice = createSlice({
  name: "modal",
  initialState: initializeModalState,
  reducers: {
    toggleShow(state, action) {
      const newState = {...state.isShow, ...action.payload}
      state.isShow = newState
    },
  },
});

export const modalActions = modalSlice.actions;

export default modalSlice.reducer;
