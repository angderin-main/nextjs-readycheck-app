import React, { useState } from "react";

const ModalContext = React.createContext({
  show: false,
  toggleHandler: () => {},
  onSubmit: () => {},
});

export const ModalContextProvider = (props) => {
  console.log("MODAL CONTEXT PROVIDER");
  const [show, setShow] = useState(false);

  const toggleHandler = () => {
    setShow(!show);
  };

  const onSubmit = () => {};

  return (
    <ModalContext.Provider
      value={{
        show: show,
        toggleHandler: toggleHandler,
        onSubmit: onSubmit,
      }}
    >
      {props.children}
    </ModalContext.Provider>
  );
};

export default ModalContext;
