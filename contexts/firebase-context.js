import React, { useEffect, useState } from "react";

import { initializeApp } from "firebase/app";
import { get, getDatabase } from "firebase/database";
import {
  browserLocalPersistence,
  browserPopupRedirectResolver,
  browserSessionPersistence,
  getAuth,
  indexedDBLocalPersistence,
  initializeAuth,
} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAci6lmByoG9Y2CvsSccCx5ZIlzAI2KwZ0",
  authDomain: "readycheck-app.firebaseapp.com",
  // The value of `databaseURL` depends on the location of the database
  databaseURL:
    "https://readycheck-app-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "readycheck-app",
  storageBucket: "readycheck-app.appspot.com",
  messagingSenderId: "46728756311",
  appId: "APP_ID",
  // For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
  measurementId: "G-MEASUREMENT_ID",
};

const FirebaseContext = React.createContext({
  firebaseApp: null,
  firebaseDb: null,
  firebaseAuth: null,
});

export const FirebaseContextProvider = (props) => {
  const app = initializeApp(firebaseConfig,"ready-check");
  const db = getDatabase(app);
  const auth = getAuth(app);
  console.log("FIREBASE-CONTEXT-PROVIDER");
  // const auth = initializeAuth(app, {
  //   persistence: [
  //     indexedDBLocalPersistence,
  //     browserLocalPersistence,
  //     browserSessionPersistence,
  //   ],
  //   popupRedirectResolver: browserLocalPersistence,
  // });
  // console.log(auth);
  // const authProvider = firebaseApp.container.getProvider("auth-exp");
  // let auth = ""
  // if (authProvider.isInitialized()) {
  //   auth = authProvider.getImmediate();
  // } else {
  //   auth = getAuth(firebaseApp);
  // }
  // const auth = getAuth(app)
  //   ? getAuth(app)
  //   : initializeAuth(app, {
  //       persistence: [
  //         indexedDBLocalPersistence,
  //         browserLocalPersistence,
  //         browserSessionPersistence,
  //       ],
  //       popupRedirectResolver: null,
  //     });

  // const [firebaseApp, setFirebaseApp] = useState(app);
  // const [firebaseDb, setFirebaseDb] = useState(getDatabase(app));
  // const [firebaseAuth, setFirebaseAuth] = useState(auth);

  return (
    <FirebaseContext.Provider
      value={{
        firebaseApp: app,
        firebaseDb: db,
        firebaseAuth: auth,
      }}
    >
      {props.children}
    </FirebaseContext.Provider>
  );
};

export default FirebaseContext;
