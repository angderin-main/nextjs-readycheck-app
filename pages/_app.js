import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";
import store from "../stores";

import Layout from "../components/containers/Layout";
import MainNavigation from "../components/containers/MainNavigation";

function MyApp({ Component, pageProps }) {
  console.log("RENDER MY APP");

  return (
    <Provider store={store}>
      <MainNavigation />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
