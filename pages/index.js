import LoginModal from "../components/auth/LoginModal";

import { useSelector, useDispatch } from "react-redux";
const HomePage = () => {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  return <>{!isLoggedIn && <LoginModal />}</>;
};

export default HomePage;
