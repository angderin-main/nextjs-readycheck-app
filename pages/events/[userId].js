import { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import useEvents from "../../hooks/use-events";
import { eventActions } from "../../stores/events";

const EventsPage = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  const { getEventListener } = useEvents();

  const onSuccessListener = useCallback((snapshot) => {
    console.log(snapshot.val());
    dispatch(eventActions.setEvents(snapshot.val()));
  }, []);

  const onErrorListener = useCallback((error) => {
    console.log(error);
  }, []);

  useEffect(() => {
    if (isLoggedIn) {
      const unsubcribe = getEventListener(onSuccessListener, onErrorListener);
      return () => unsubcribe();
    }
  }, [onSuccessListener, onErrorListener, getEventListener, isLoggedIn]);

  return <></>;
};

export default EventsPage;
