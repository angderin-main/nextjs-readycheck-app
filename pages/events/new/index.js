import { Button } from "react-bootstrap";
import NewEventsForm from "../../../components/events/NewEventForm";
import useEvents from "../../../hooks/use-events";
import { initializeEventState } from "../../../stores/events";

const NewEventsPage = () => {
  // const { addEventHandler } = useEvents();

  const onSuccestEvent = () => {
    console.log("SUCCESS");
  };

  const onErrorEvent = (error) => {
    console.log(error);
  };

  // const newEventHandler = () => {
  //   let newEvent = initializeEventState;
  //   newEvent.title = "Testing 123";
  //   addEventHandler(newEvent, onSuccestEvent, onErrorEvent);
  // };

  return (
    <div className="mt-5">
      {/* <Button onClick={newEventHandler}> ADD </Button> */}
      <Button> ADD </Button>
      <NewEventsForm></NewEventsForm>
    </div>
  );
};

export default NewEventsPage;
