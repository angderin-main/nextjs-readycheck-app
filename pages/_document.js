import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html>
      <Head />
      <body style={{backgroundColor: "#424B54"}}>
        <div id="modal" />
        <Main/>
        <NextScript />
      </body>
    </Html>
  )
}