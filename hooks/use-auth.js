import {
  GoogleAuthProvider,
  signInWithPopup,
  signInWithEmailAndPassword,
  signOut,
  createUserWithEmailAndPassword,
} from "firebase/auth";
import { useDispatch } from "react-redux";
import { authActions } from "../stores/auth";
import useFirebase from "./use-firebase";

export const AUTH_ACTIONS = {
  GOOGLE: "GOOGLE",
  PASSWORD: "PASSWORD",
  CREATE_PASSWORD: "CREATE_PASSWORD",
};

const useAuth = () => {
  const { firebaseAuth: auth } = useFirebase();

  const dispatch = useDispatch();

  const loginHandler = (
    type,
    data = {},
    onSuccess = () => {},
    onError = (error) => {}
  ) => {
    switch (type) {
      case AUTH_ACTIONS.GOOGLE:
        console.log("LOGIN GOOGLE");
        const provider = new GoogleAuthProvider();
        signInWithPopup(auth, provider)
          .then(() => {
            dispatch(authActions.isLogin());
            onSuccess();
          })
          .catch((error) => {
            console.log("HANDLER LOGIN ERROR", error);
            onError(error);
          });
        break;
      case AUTH_ACTIONS.PASSWORD:
        console.log("LOGIN PASSWORD");
        signInWithEmailAndPassword(auth, data.email, data.password)
          .then(() => {
            dispatch(authActions.isLogin());
            onSuccess();
          })
          .catch((error) => {
            console.log("HANDLER LOGIN ERROR", error);
            onError(error);
          });
        break;
      case AUTH_ACTIONS.CREATE_PASSWORD:
        console.log("CREATE PASSWORD");
        createUserWithEmailAndPassword(auth, data.email, data.password)
          .then(() => {
            dispatch(authActions.isLogin());
            onSuccess();
          })
          .catch((error) => {
            console.log("HANDLER LOGIN ERROR", error);
            onError(error);
          });
        break;
      default:
        break;
    }
  };

  const logoutHandler = () => {
    signOut(auth)
      .then(() => {
        dispatch(authActions.isLogout());
      })
      .catch((error) => console.log("HANDLER LOGOUT ERROR", error));
  };

  return { loginHandler, logoutHandler };
};

export default useAuth;
