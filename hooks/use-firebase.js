import { initializeApp, getApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyAci6lmByoG9Y2CvsSccCx5ZIlzAI2KwZ0",
  authDomain: "readycheck-app.firebaseapp.com",
  // The value of `databaseURL` depends on the location of the database
  databaseURL:
    "https://readycheck-app-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "readycheck-app",
  storageBucket: "readycheck-app.appspot.com",
  messagingSenderId: "46728756311",
  appId: "APP_ID",
  // For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
  measurementId: "G-MEASUREMENT_ID",
};

const useFirebase = () => {
  let app = null;
  try {
    app = getApp("ready-check");
  } catch (error) {
    app = initializeApp(firebaseConfig, "ready-check");
  }
  
  if (app.name === "[DEFAULT]") {
    app = initializeApp(firebaseConfig, "ready-check");
  }
  const db = getDatabase(app);
  const auth = getAuth(app);

  return { firebaseApp: app, firebaseDb: db, firebaseAuth: auth };
};

export default useFirebase;
