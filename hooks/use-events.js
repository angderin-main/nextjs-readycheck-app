import { onValue, push, ref, set } from "firebase/database";
import { initializeEventState } from "../stores/events";
import { useSelector } from "react-redux";
import useFirebase from "./use-firebase";

const useEvents = () => {
  const { firebaseAuth: auth, firebaseDb: db } = useFirebase();
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  // console.log(auth);
  let uid = "";
  if (isLoggedIn) {
    uid = auth.currentUser.uid;
  }

  const eventListRef = ref(db, "events/" + uid);

  const getEventListener = (
    onSuccess = (snapshot) => {},
    onError = (error) => {}
  ) => {
    return onValue(eventListRef, onSuccess, onError);
  };

  const addEventHandler = (
    newEventData = initializeEventState,
    onSuccess = () => {},
    onError = (error) => {}
  ) => {
    const newEventRef = push(eventListRef);
    set(newEventRef, newEventData).then(onSuccess).catch(onError);
  };

  return { getEventListener, addEventHandler };
};

export default useEvents;
