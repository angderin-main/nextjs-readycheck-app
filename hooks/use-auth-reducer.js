import { useReducer } from "react";

export const initValue = {
  isLoggedIn: false,
  userData: {
    credential: null,
    token: null,
    user: null,
  },
  onLogout: () => {},
  onLogin: (TYPE, options = {}) => {},
};

const reducer = (state, action) => {
  console.log("REDUCER");
  alert();
  if (action.type === "AUTH_GOOGLE") {
    console.log("SIGNIN WITH POPUP");
    console.log(action);
    alert();
    return { ...state };
    // signInWithPopup(action.auth, provider).then((result) => {
    //     console.log("HANDLER LOGIN RESULT", result);

    //     const resultCredential =
    //       GoogleAuthProvider.credentialFromResult(result);

    //     const resultUserData = {
    //       isLoggedIn: true,
    //       userData: {
    //         credential: resultCredential,
    //         token: resultCredential.accessToken,
    //         user: result.user,
    //       },
    //     };
    //     return resultUserData;
    //   }).catch((error) => {
    //     console.log("HANDLER LOGIN ERROR", error);
    //   });
  }

  if (action.type === "AUTH_LOGOUT") {
    return {
      isLoggedIn: false,
      userData: {
        credential: null,
        token: null,
        user: null,
      },
    };
  }

  return { ...state };
};

export function useApiCallReducer() {
  return useReducer(reducer, initValue);
}